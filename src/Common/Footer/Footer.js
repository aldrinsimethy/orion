import React from 'react'
import './Footer.css';
import logo from '../../assets/images/logo.png'

import {
  Link
} from "react-router-dom";

function Footer() {
  return (
    <div>
      <footer>
          <div className='container'>
            <div className='row'>
              <div className='col-md-4'>
                <img src={logo}/>
              </div>
              <div className='col-md-4'>
                <h5>About us</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisl orci a morbi a, iaculis risus odioLorem ipsum dolor sit amet, consecte</p>
              </div>
              <div className='col-md-2'>
                <h5>Quick Links</h5>
                <p>Test Content</p>
                <p>Test Content</p>
                <p>Test Content</p>
              </div>
              <div className='col-md-2'>
                <h5>Quick Links</h5>
                <p>Test Content</p>
                <p>Test Content</p>
                <p>Test Content</p>
              </div>
            </div>
          </div>
      </footer>

      <section className='footer-bottom'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-10'>
              <p>© 2021. Orion Study Abroad Pvt. Ltd. All Rights Reserved.&nbsp; &nbsp; | &nbsp; &nbsp;Developed by  Noviindus</p>
            </div>
            <div className='col-md-2'>
            <Link to="/" className='top'></Link>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default Footer