import React from 'react';
import './Accordian.css'
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';

// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css';

export default function CustomAccordion() {
    return (
        <Accordion>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        <h6 className='acc-title'>Jobseeker Visa in Germany</h6>
                        What harsh truths do you prefer to ignore?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>
                    Orion provides guidance on Jobseeker Visa for Engineers, IT Professionals, ...
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading className='bg-none'>
                    <AccordionItemButton>
                    <h6 className='acc-title'>Jobseeker Visa in Germany</h6>
                        What harsh truths do you prefer to ignore?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <p>
                    Orion provides guidance on Jobseeker Visa for Engineers, IT Professionals, ...
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                    <h6 className='acc-title'>Jobseeker Visa in Germany</h6>
                        What harsh truths do you prefer to ignore?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <p>
                    Orion provides guidance on Jobseeker Visa for Engineers, IT Professionals, ...
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                    <h6 className='acc-title'>Jobseeker Visa in Germany</h6>
                        What harsh truths do you prefer to ignore?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <p>
                    Orion provides guidance on Jobseeker Visa for Engineers, IT Professionals, ...
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
        </Accordion>
    );
}