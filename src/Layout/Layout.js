import React from 'react'
import {
    Routes,
    Route,
  } from "react-router-dom";
  import Header from "../Common/Header/Header"
import Footer from "../Common/Footer/Footer"
import Home from "../Home/Home"

function Layout() {
  return (
    <div>
        <Header></Header>
        <Home></Home>
        <Footer></Footer>
    </div>
  )
}

export default Layout