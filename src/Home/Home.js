import React from 'react'
import './Home.css'
import {
    Link
  } from "react-router-dom";

import flight from '../assets/images/flight.png'
import trophy from '../assets/images/trophy.png'
import customer from '../assets/images/Group.png'
import image1 from '../assets/images/img1.png'
import image2 from '../assets/images/img2.png'

import gallery1 from '../assets/images/gal1.png'
import gallery2 from '../assets/images/gal2.png'
import gallery3 from '../assets/images/gal3.png'
import gallery4 from '../assets/images/gal4.png'
import gallery5 from '../assets/images/gal5.png'
import gallery6 from '../assets/images/gal6.png'

import right from '../assets/images/right_img.png'
import primehero from '../assets/images/prime_hero.png'

import flag1 from '../assets/images/flg1.png'
import flag2 from '../assets/images/flg2.png'
import flag3 from '../assets/images/flg3.png'
import flag4 from '../assets/images/flg4.png'
import flag5 from '../assets/images/flg5.png'
import flag6 from '../assets/images/flg6.png'
import flag7 from '../assets/images/flg7.png'
import rightarrow from '../assets/images/right-arrow.png'

import line from '../assets/images/group24.png'

import arrowbtn from '../assets/images/arrow_btn.png'
import CustomAccordion from '../Common/Accordian/Accordian';

function Home() {
  return (
    <div>
        <section className='banner'>
            <div className='banner-container'>
                <div className='row'>
                    <div className='col-md-5'>
                        <h1>Dreams come to Reality..</h1>
                        <p>We Make Studying Abroad Easier...</p>
                        <button className='read-btn'>Read More</button>
                    </div>
                    <div className='col-md-7'></div>
                </div>
            </div>
            <div className='web-list'>
            <div className='banner-list'>
                <div className='row'>
                    <div className='col-md-6'></div>
                    <div className='col-md-6'>
                        <div className='list-content blue'>
                            <div className='image white'>
                                <img src={flight} />
                            </div>
                            <h5>Fly to your dream study Destination</h5>
                        </div>                        
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='list-content'>
                            <div className='image'>
                                <img src={trophy} />
                            </div>
                            <h5>study abord with the industry leaders</h5>
                        </div>
                    </div>
                    <div className='col-md-6'></div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        
                    </div>
                    <div className='col-md-6'>
                        <div className='hero'>
                            <img src={image1} />
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='list-content'>
                            <div className='image'>
                                <img src={customer} />
                            </div>
                            <h5>Life changing support by Orion</h5>
                        </div>
                    </div>
                    <div className='col-md-6'>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        
                    </div>
                    <div className='col-md-6'>
                        <div className='hero'>
                            <img src={image2} />
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>        

        <section className='study'>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-7'>
                        <h2>We Make <br/>Studying Abroad Easier..</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisl orci a morbi a, iaculis risus odio fermentum. Nullam egestas duis malesuada viverra diam dis habitasse. Netus gravida id dignissim a varius scelerisque mauris posuere elit. Pulvinar vestibulum elementum tempor lectus  eget amet. Vel nisl ut id at fermentum amet at id odio. Aliqulacus, nibh ridiculus cursus euismod. Nullam sed in eu id eget justo adipiscing.
<br/>Ipsum lacus aliquet at magna est egestas sit risus nullam. Massa morbi fermentum pellentesque eget amet. Vel nisl ut id at fermentum amet at id odio. Aliquam suscipit pretium, viverra sed ac sodales in lectus. Lectus dolor viver eget amet. Vel nisl ut id at fermentum amet at id odio</p>
                    </div>
                    <div className='col-md-5'></div>
                </div>
            </div>
        </section>

        <section className='mob-list'>
        <div className='banner-list'>
                <div className='row'>
                    <div className='col-md-6'></div>
                    <div className='col-md-6'>
                        <div className='list-content blue'>
                            <div className='image white'>
                                <img src={flight} />
                            </div>
                            <h5>Fly to your dream study Destination</h5>
                        </div>                        
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='list-content'>
                            <div className='image'>
                                <img src={trophy} />
                            </div>
                            <h5>study abord with the industry leaders</h5>
                        </div>
                    </div>
                    <div className='col-md-6'></div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        
                    </div>
                    <div className='col-md-6'>
                        <div className='hero'>
                            <img src={image1} />
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='list-content'>
                            <div className='image'>
                                <img src={customer} />
                            </div>
                            <h5>Life changing support by Orion</h5>
                        </div>
                    </div>
                    <div className='col-md-6'>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        
                    </div>
                    <div className='col-md-6'>
                        <div className='hero'>
                            <img src={image2} />
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className='offer'>
            <div className='container'>
                <ul className='offer-list'>
                    <li>
                        <h3>What <br/>we offer</h3>
                        <p className='ofr-txt'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisl orci a morbi a, iaculis risus odio </p>
                    </li>
                    <li>
                        <div className='offer-list-text'>
                            <h6>IELTS, TOEFL, PTE and German Language Exam Guidance.</h6>
                        </div>                                                
                    </li>
                    <li>
                        <Link to='/'><img src={arrowbtn}/></Link>
                        <div className='offer-list-text'>                            
                            <h6>Study and Career Counselling</h6>
                        </div>                                                
                    </li>
                    <li>
                        <Link to='/'><img src={arrowbtn}/></Link>
                        <div className='offer-list-text'>                            
                            <h6>Admission support for universities worldwide</h6>
                        </div>                                                
                    </li>
                    <li>
                        <Link to='/'><img src={arrowbtn}/></Link>
                        <div className='offer-list-text'>                            
                            <h6>Visa support for all categories</h6>
                        </div>                                                
                    </li>
                    <li>
                        <Link to='/'><img src={arrowbtn}/></Link>
                        <div className='offer-list-text'>                            
                            <h6>Travel and post arrival assistance</h6>
                        </div>                                                
                    </li>
                </ul>

                <div className='row'>
                    <div className='col-md-12'>
                        <div className='line'>
                            <img src={line}/>
                        </div>
                    </div>
                </div>

                <div className='bottom-text'>
                    <div className='row'>
                        <div className='col-md-8'>
                            <h5>Why wait ?  lets find your dream destination</h5>
                        </div>
                        <div className='col-md-4'>
                            <button className='meeting-btn'>Schedule a Meeting</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className='contries'>
            <div className='container'>
                <h3>Contries</h3>
                <p>Orion Study Abroad Pvt Ltd is a mentor for all students aspiring to study abroad in countries like Germany, Canada, USA, UK, Sweden, UAE, Australia, NZ, and other countries offering high-quality education along with unique learning environment.</p>
                <ul className='contry-list'>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>Germany</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag1} />                                
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>Canada</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag2} />                                
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>USA</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag3} />                                
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>United Kingdom</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag4} />                                
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>Sweden</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag5} />                                
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>Australia</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag6} />                                
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>UAE</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag7} />                                
                            </div>
                        </Link>
                    </li>
                    <li>
                        <Link to='/'>
                            <div className='text-area'>
                                <h6>Germany</h6>
                                <img src={rightarrow} />
                                <p>View all</p>
                                <strong>Our services</strong>
                            </div>
                            <div className='image-area'>
                                <img src={flag1} />                                
                            </div>
                        </Link>
                    </li>
                </ul>
            </div>
        </section>

        <section className='orion-prime'>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                        <h3>Orion Prime</h3>
                        <p>Those who wish to partner with us in working towards developing overseas education would leverage Orion's network of universities and other programs to create a substantiate income by referrals.</p>
                        <button className='prime-btn'>Join Orion Prime</button>
                    </div>
                    <div className='col-md-6'>
                        <div className='prime-right'>
                            <div className='right-image'>
                                <img src={primehero}/>
                            </div>
                            <div className='bubble-box'>
                                <h6>Join Prime! &nbsp; &nbsp; &nbsp; »</h6>
                                <Link to='/' className='call'>Contact : +91 9895556166</Link><br/>
                                <Link to='/' className='mail'>Mail :prime@orionstudyabroad.com</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className='whats-new'>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-8'>
                        <div className='left-wrap'>
                            <h3>What's new</h3>
                            <p>Orion Study Abroad Pvt Ltd is a mentor for all students</p>
                            <CustomAccordion></CustomAccordion>
                        </div>
                    </div>
                    <div className='col-md-4'>
                        <div className='right-wrap'>
                            <img src={right}/>
                            <div className='right-wrap-text'>
                                <h5>IELTS, TOEFL, PTE and German Language Exam Guidance..</h5>
                                <button className='red-mre'>Read more</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className='gallery'>
            <div className='container'>
                <h3>Gallery</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisl orci a morbi a, iaculis risus odio</p>
                <div className='row'>
                    <div className='col-md-6'>
                        <img src={gallery1} />
                    </div>
                    <div className='col-md-3'>
                        <img src={gallery2} />
                    </div>
                    <div className='col-md-3'>
                        <img src={gallery3} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6'>
                        <img src={gallery4} />
                    </div>
                    <div className='col-md-3'>
                        <img src={gallery5} />
                    </div>
                    <div className='col-md-3'>
                        <img src={gallery6} />       
                    </div>
                </div>
            </div>
        </section>

        <section className='help-desk'>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                        <div className='help-desk-left'>
                            <p>HELP DESK 24H/7</p>
                            <Link to="/">+91  983 766 284 25</Link>
                        </div>
                    </div>
                    <div className='col-md-6'>
                        <div className='help-desk-right'>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nisl orci a morbi a, iaculis risus odioLorem ipsum dolor sit amet, consectetur adipiscing elit. Nisl orci a morbi a, iaculis risus odioLorem ipsum dolor sit amet, consectetur adipiscin</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
  )
}

export default Home