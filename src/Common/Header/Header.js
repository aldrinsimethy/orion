import React from 'react'
import './Header.css'
import {
    Link
  } from "react-router-dom";

import logo from '../../assets/images/logo.png'
function Header() {
  return (
    <>
    <div className='header-outer'>
        <div className='header-container'>
            <div className='row'>
                <div className='col-md-3'>
                    <Link to='/' className='logo'><img src={logo} /></Link>
                </div>
                <div className='col-md-9'>
                    <ul class="menu">
                        <li><Link to="/">HOME</Link></li>
                        <li><Link to="/">WHY ORION</Link></li>
                        <li><Link to="/">WHAT WE DO</Link></li>
                        <li><Link to="/">MBBS</Link></li>
                        <li><Link to="/">COUNTRIES</Link></li>
                        <li><Link to="/">ORION PRIME</Link></li>
                        <li><Link to="/">WHATS NEW</Link></li>
                        <li><button className='cont-btn'>Contact</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<header className='mobile-menu'>
<div className='header-container'>
    <div class="menu-content">
        <div class="logo">
        <Link to='/' className='logo'><img src={logo} /></Link>
        </div>

        {/* <a href='#' className='sign-btn'>LOGIN / SIGNUP</Link> */}
        <nav>
            <input type="checkbox" id="hamburger1" />
            <label for="hamburger1"></label>

            <ul class="nav-links">
                <li><Link to="/">HOME</Link></li>
                <li><Link to="/">WHY ORION</Link></li>
                <li><Link to="/">WHAT WE DO</Link></li>
                <li><Link to="/">MBBS</Link></li>
                <li><Link to="/">COUNTRIES</Link></li>
                <li><Link to="/">ORION PRIME</Link></li>
                <li><Link to="/">WHATS NEW</Link></li>
            </ul>
        </nav>
    </div>

</div>
</header>
</>
  )
}

export default Header